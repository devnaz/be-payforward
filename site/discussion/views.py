from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseBadRequest, HttpResponseRedirect
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect
from .models import *
from .forms import CommentForm

@login_required(login_url=reverse_lazy('home'))
def topics(request):
    topics = Topic.objects.filter(published=True).order_by('-date')
    return render(request, 'discussion/topics.html', {'topics': topics})


@login_required(login_url=reverse_lazy('home'))
def topic_view(request, pk):
    topic = get_object_or_404(Topic, id=pk)
    comments = Comment.objects.filter(topic_id=topic.id)
    form = CommentForm()
    return render(request, 'discussion/topic_view.html', {'topic': topic,
                                                          'comments': comments,
                                                          'form': form})


@csrf_protect
def add_comment(request, pk):
    if request.POST:
        topic = get_object_or_404(Topic, id=pk)
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.user = request.user
            comment.topic_id = topic.id
            comment.save()
            return HttpResponseRedirect(reverse('topic_view', args=[topic.id]))
        else:
            topic = get_object_or_404(Topic, id=pk)
            comments = Comment.objects.filter(topic_id=topic.id)
            form = CommentForm(request.POST)
            return render(request, 'discussion/topic_view.html', {'topic': topic,
                                                                  'comments': comments,
                                                                  'form': form})
    return HttpResponseBadRequest('Error')
