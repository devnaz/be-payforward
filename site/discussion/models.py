from django.db import models
from django.contrib.auth.models import User


class Topic(models.Model):
    name = models.CharField('Назва теми', max_length=100)
    text = models.TextField('Опис теми')
    date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    published = models.BooleanField('Опублікувати', default=True)

    class Meta:
        verbose_name = 'Тема'
        verbose_name_plural = 'Теми'

    def __str__(self):
        return self.name


class Comment(models.Model):
    text = models.TextField()
    date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    user = models.ForeignKey(User)
    topic = models.ForeignKey(Topic)
