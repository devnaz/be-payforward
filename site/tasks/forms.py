from django import forms
from .models import Task, Response
from random import randint


class CreateTaskFrom(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['name', 'text', 'category', 'image']

    def clean_image(self):
        img = self.cleaned_data['image']
        if img is None or img == '':
            img = '../static/images/layout/tasks/img%s.jpg' %(randint(1, 3))
        return img



class AddResponseForm(forms.ModelForm):
    class Meta:
        model = Response
        fields = ['text']

    def clean_text(self):
        text = self.cleaned_data['text']
        if len(text) < 2:
            raise forms.ValidationError('Слишком короткий текст для отклика.')
        return text
