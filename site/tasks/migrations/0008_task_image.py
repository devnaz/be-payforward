# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-02-16 08:47
from __future__ import unicode_literals

from django.db import migrations, models
import tasks.models


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0007_auto_20170216_1031'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='image',
            field=models.ImageField(default=1, upload_to=tasks.models.path_image),
            preserve_default=False,
        ),
    ]
