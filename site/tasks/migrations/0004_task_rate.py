# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-02-16 08:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0003_task_date_create'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='rate',
            field=models.IntegerField(default=1, validators=[1, 10], verbose_name='Рейтинг від 1 до 10'),
        ),
    ]
