# coding = utf-8

from django.shortcuts import render, render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest
from django.core.urlresolvers import reverse
# from django.contrib.auth.decorators import login_required
from django.db.models import Q
from .models import *
from .forms import CreateTaskFrom, AddResponseForm


def home(request):
    grand_tasks = Task.objects.exclude(user_id=request.user.id).order_by('-rate')[:4]
    fresh_tasks = Task.objects.exclude(user_id=request.user.id).order_by('-date_create')[:4]
    task_form = CreateTaskFrom()
    sliders = Slider.objects.filter(published=True).order_by('-date')
    return render(request, 'index.html', {'grand_tasks': grand_tasks,
                                          'fresh_tasks': fresh_tasks,
                                          'task_form': task_form,
                                          'sliders': sliders})


def create_task(request):
    if request.POST:
        form = CreateTaskFrom(request.POST, request.FILES)
        if form.is_valid():
            task = form.save(commit=False)
            task.user = request.user
            task.save()
            return HttpResponseRedirect(reverse('task_view', args=[task.id]))
    return HttpResponseBadRequest()


def tasks(request):
    all_tasks = Task.objects.exclude(user_id=request.user.id).order_by('-date_create')  # exclude(user_id=request.user.id) не виводить задачи которие относятся к залогиненому пользователю
    return render(request, 'tasks/tasks.html', {'all_tasks': all_tasks})


def search_tasks(request):
    if request.POST:
        search_text = request.POST.get('search')
    else:
        search_text = ''
    filter_tasks = Task.objects.exclude(user_id=request.user.id).filter(name__contains=search_text).order_by('-date_create')
    return render(request, 'tasks/filter_tasks.html', {'filter_tasks': filter_tasks})


def task_view(request, pk):
    task = get_object_or_404(Task, id=pk)
    some_tasks = Task.objects.exclude(Q(user_id=request.user.id) | Q(id=pk)).order_by('?')[:3]  # exclude(id=pk) - exclude it's not self obj
    responses = Response.objects.filter(task_id=task.id)
    form = AddResponseForm()
    return render(request, 'tasks/task_view.html', {'task': task,
                                                    'some_tasks': some_tasks,
                                                    'responses': responses,
                                                    'form': form})


def add_response(request, pk):
    if request.POST:
        task = get_object_or_404(Task, id=pk)
        form = AddResponseForm(request.POST)
        if form.is_valid():
            resp = form.save(commit=False)  # commit false создан для того что би прежде чем сохранить дание в БД можна било б добавить другие поля і тогда все сохранить
            resp.task_id = task.id
            resp.user = request.user
            resp.save()
            return HttpResponseRedirect(reverse('task_view', args=[task.id]))
        else:
            task = get_object_or_404(Task, id=pk)
            some_tasks = Task.objects.exclude(id=pk).order_by('?')[:3]  # exclude it's not self obj
            responses = Response.objects.filter(task_id=pk)
            form = AddResponseForm(request.POST)
            return render(request, 'tasks/task_view.html', {'task': task,
                                                            'some_tasks': some_tasks,
                                                            'responses': responses,
                                                            'form': form})
    return HttpResponseBadRequest('Error')
