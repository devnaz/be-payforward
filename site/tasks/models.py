# coding = utf-8

from django.db import models
from django.contrib.auth.models import User


RATE_CHOICES = [(i, i) for i in range(1, 11)]


def path_image(instance, filename):
    return 'tasks/%s' %(filename)


class Slider(models.Model):
    title = models.CharField('Заголовок слайдера', max_length=50)
    desc = models.TextField('Опис слайдера')
    img = models.ImageField(upload_to='sliders', verbose_name='Картинка слайдера')
    date = models.DateField(auto_now_add=True, blank=True, null=True)
    published = models.BooleanField('Отображение слайдера', default=True)


class Category(models.Model):
    name = models.CharField('Назва категорії', max_length=50)

    class Meta:
        verbose_name = 'Категорія'
        verbose_name_plural = 'Категорії'

    def __str__(self):
        return self.name


class Task(models.Model):
    name = models.CharField('Назва задачі', max_length=100)
    text = models.TextField('Опис задачі')
    date_create = models.DateTimeField(auto_now_add=True)
    rate = models.IntegerField('Рейтинг', default=1, choices=RATE_CHOICES)
    image = models.ImageField(upload_to=path_image, blank=True)
    # date_limit = models.DateTimeField()
    category = models.ForeignKey(Category, verbose_name='Категорія')
    user = models.ForeignKey(User, verbose_name='Автор задачі')

    class Meta:
        verbose_name = 'Задача'
        verbose_name_plural = 'Задачі'

    def __str__(self):
        return self.name


class Response(models.Model):
    text = models.TextField('Текст для отклика')
    date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    task = models.ForeignKey(Task)
    user = models.ForeignKey(User)
