from django import forms
from .models import UserProfile


class RegisterForm(forms.Form):
    name = forms.CharField(
        label='Логин'
    )
    password = forms.CharField(
        widget=forms.PasswordInput(),
        label='Пароль'
    )
    filename = forms.FileField(
        label='Фотография',
        required=False
    )


# class LoginForm(forms.Form):
#     name = forms.CharField(
#         label='Логин'
#     )
#     password = forms.CharField(
#         widget=forms.PasswordInput(),
#         label='Пароль'
#     )
