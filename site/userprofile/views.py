from django.shortcuts import render, get_object_or_404
from django.contrib import auth
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse, reverse_lazy
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from .models import UserProfile
from .forms import RegisterForm
from os import path

DIR_NAME = path.dirname(__name__)

@csrf_protect
def register(request):
    form = RegisterForm()
    if request.POST:
        form = RegisterForm(request.POST, request.FILES)
        if form.is_valid():
            data = form.cleaned_data
            name = data['name']
            password = data['password']
            filename = data['filename']

            if filename is None or filename == '':
                filename = path.join(DIR_NAME, '../static/images/layout/anonym-user.png')

            # create new user
            user = User.objects.create_user(username=name, password=password)
            user.save()

            UserProfile.objects.create(
                user = user,
                avatar = filename
            )
            auth.login(request, user)
            return HttpResponseRedirect(reverse('profile', args=[user.id]))
        else:
            form = RegisterForm(request.POST)
            return render(request, 'userprofile/registration.html', {'form': form})
    else:
        return render(request, 'userprofile/registration.html', {'form': form})


@csrf_protect
def login(request):
    if request.POST:
        name = request.POST.get('name')
        password = request.POST.get('password')
        user = auth.authenticate(username=name, password=password)
        if user is not None and user.is_active:
            # A backend authenticated the credentials
            auth.login(request, user)
            return HttpResponseRedirect(reverse('profile', args=[user.id]))
    return HttpResponseRedirect(reverse('home'))


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('home'))


def people(request):
    users = User.objects.exclude(id=request.user.id)
    return render(request, 'userprofile/people.html', {'users': users})


def search_peole(request):
    if request.POST:
        search_text = request.POST.get('search')
        filter_people = User.objects.exclude(id=request.user.id).filter(username__contains=search_text)
    return render(request, 'userprofile/search_peole.html', {'filter_people': filter_people})


@login_required(login_url=reverse_lazy('home'))
def profile(request, pk):
    user = get_object_or_404(User, id=pk)
    all_tasks = Task.objects.filter(user_id=user.id)
    tasks = Task.objects.filter(user_id=user.id).order_by('?')[:3]
    return render(request, 'userprofile/profile.html', {'user': user,
                                                        'tasks': tasks,
                                                        'all_tasks': all_tasks})
