"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from tasks.views import *
from userprofile.views import *
from discussion.views import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^about/', include('django.contrib.flatpages.urls')),

    url(r'^$', home, name='home'),

    url(r'^tasks/$', tasks, name='tasks'),
    url(r'^tasks/(?P<pk>\d+)/$', task_view, name='task_view'),
    url(r'^add_response/(?P<pk>\d+)/$', add_response, name='add_response'),
    url(r'^create_task/$', create_task, name='create_task'),
    url(r'^search_tasks/$', search_tasks, name='search_tasks'),


    # userprofile
    url(r'^account/register/$', register, name='register'),
    url(r'^account/login/$', login, name='login'),
    url(r'^account/logout/$', logout, name='logout'),

    url(r'^people/$', people, name='people'),
    url(r'^search_peole/$', search_peole, name='search_peole'),
    url(r'^people/(?P<pk>\d+)/$', profile, name='profile'),

    url(r'^discussion/topics/$', topics, name='topics'),
    url(r'^discussion/topics/(?P<pk>\d+)/$', topic_view, name='topic_view'),
    url(r'^discussion/add_comment/(?P<pk>\d+)/$', add_comment, name='add_comment'),


] + static(
    settings.STATIC_URL, document_root=settings.STATIC_ROOT
) + static(
    settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
)
